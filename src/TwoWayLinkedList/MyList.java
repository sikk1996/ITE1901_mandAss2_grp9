package TwoWayLinkedList;

// MyList from 11th edition

import java.util.*;

public interface MyList<E> extends Collection<E> {
	/**
	 * Add a new element at the specified index in this list
	 */
	public void add(int index, E e);

	/**
	 * Return the element from this list at the specified index
	 */
	public E get(int index);

	/**
	 * Return the index of the first matching element in this list.
	 * Return -1 if no match.
	 */
	public int indexOf(Object e);

	/**
	 * Return the index of the last matching element in this list
	 * Return -1 if no match.
	 */
	public int lastIndexOf(E e);

	/**
	 * Remove the element at the specified position in this list
	 * Shift any subsequent elements to the left.
	 * Return the element that was removed from the list.
	 */
	public E remove(int index);

	/**
	 * Replace the element at the specified position in this list
	 * with the specified element and returns the new set.
	 */
	public E set(int index, E e);

	@Override
	/**
	 * Add a new element at the end of this list
	 */
	public default boolean add(E e) {
		add(size(), e);
		return true;
	}

	@Override
	/**
	 * Return true if this list contains no elements
	 */
	public default boolean isEmpty() {
		return size() == 0;
	}

	@Override
	/**
	 * Remove the first occurrence of the element e
	 * from this list. Shift any subsequent elements to the left.
	 * Return true if the element is removed.
	 */
	public default boolean remove(Object e) {
		if (indexOf(e) >= 0) {
			remove(indexOf(e));
			return true;
		} else
			return false;
	}

	@Override
	public default boolean containsAll(Collection<?> c) {
		int numberOfElements = 0;
		for (Object e : c) {
			if (contains(e))
				numberOfElements++;
		}
		return numberOfElements == c.size();
	}

	@Override
	public default boolean addAll(Collection<? extends E> c) {
		boolean collectionChanged = false;
		for (E e : c) {
			add(e);
			collectionChanged = true;
		}
		return collectionChanged;
	}

	@Override
	public default boolean removeAll(Collection<?> c) {
		boolean isModified = false;
		for (Object e : c) {
			if (contains(e)) {
				remove(e);
				isModified = true;
			}
		}
		return isModified;
	}

	@Override
	public default boolean retainAll(Collection<?> c) {
		boolean isModified = false;
		for (int i = size() - 1; i >= 0; i--) {
			Object obj = get(i);
			if (!c.contains(obj)) {
				remove(i);
				isModified = true;
			}
		}

		return isModified;
	}

	@Override
	public default Object[] toArray() {
		Object[] array = new Object[size()];
		for (int i = 0; i < size(); i++)
			array[i] = get(i);

		return array;
	}

	@Override
	public default <T> T[] toArray(T[] array) {
		Class type = array.getClass().getComponentType();
		Object[] listArray = toArray();

		for (int i = 0; i < size(); i++) {
			if (!type.isInstance(get(i))) {
				throw new ArrayStoreException("Runtime type of the specified array is not a supertype of " +
						"the runtime type of every element in this collection");
			}
		}

		if (array.length < size()) {
			array = (T[]) Arrays.copyOf(listArray, size(), array.getClass());
		}

		return array;
	}
}
