package TwoWayLinkedList;

import java.util.*;

public class TwoWayLinkedList<E> implements MyList<E> {
	private Node<E> head, tail;
	private int size = 0;

	/**
	 * Create a default list
	 */
	public TwoWayLinkedList() {
	}

	/**
	 * Create a list from an array of objects
	 */
	public TwoWayLinkedList(E[] objects) {
		this.addAll(Arrays.asList(objects));
	}

	/**
	 * Return the head element in the list
	 */
	public E getFirst() {
		if (size == 0) {
			throw new NoSuchElementException("No elements in list");
		} else {
			return head.element;
		}
	}

	/**
	 * Return the last element in the list
	 */
	public E getLast() {
		if (size == 0) {
			throw new NoSuchElementException("No elements in list");
		} else {
			return tail.element;
		}
	}

	/**
	 * Add a new element at the specified index in this list The index of the
	 * head element is 0
	 */
	public void add(int index, E e) {
		if (index > size || index < 0) throw new IndexOutOfBoundsException();
		else if (index == 0) addFirst(e);
		else if (index == size) addLast(e);
		else {
			Node<E> current;
			if (index > size / 2) {
				current = tail;
				for (int i = size - 1; i > index; i--)
					current = current.previous;
			} else {
				current = head;

				for (int i = 0; i < index; i++) {
					current = current.next;
				}
			}
			Node<E> newNode = new Node<>(e);
			Node<E> temp = current.previous;

			current.previous.next = newNode;
			current.previous = newNode;
			newNode.previous = temp;
			newNode.next = current;
			size++;
		}
	}

	/**
	 * Remove the element at the specified position in this list. Return the
	 * element that was removed from the list.
	 */
	public E remove(int index) {
		if (index >= size || index < 0) throw new IndexOutOfBoundsException();
		else if (index == 0) return removeFirst();
		else if (index == size - 1) return removeLast();
		else {
			Node<E> current;

			if (size / 2 <= index) { //start i korteste ende.
				current = tail;
				for (int i = size - 2; i >= index; i--)
					current = current.previous;
			} else {
				current = head;
				for (int i = 1; i <= index; i++)
					current = current.next;
			}
			current.previous.next = current.next;
			current.next.previous = current.previous;
			size--;
			return current.element;

		}
	}

	/**
	 * Add an element to the beginning of the list
	 */
	public void addFirst(E e) {
		if (head == null) head = tail = new Node<>(e);
		else {
			head.previous = new Node<>(e);
			head.previous.next = head;
			head = head.previous;
		}
		size++;
	}

	/**
	 * Add an element to the end of the list
	 */
	public void addLast(E e) {
		if (tail == null) addFirst(e);
		else {
			tail.next = new Node<>(e);
			tail.next.previous = tail;
			tail = tail.next;
			size++;
		}
	}

	/**
	 * Remove the head node and return the object that is contained in the
	 * removed node.
	 */
	public E removeFirst() {
		if (head == null) throw new NoSuchElementException();
		else if (size == 1) return removeLast();
		else {
			Node<E> tempNode = head;
			head = head.next;
			head.previous = null;
			--size;
			return tempNode.element;
		}
	}

	/**
	 * Remove the last node and return the object that is contained in the
	 * removed node.
	 */
	public E removeLast() {
		if (tail == null) throw new NoSuchElementException();
		else if (size == 1) {
			E element = head.element;
			head = tail = null;
			--size;
			return element;
		} else {
			Node<E> tempNode = tail;
			tail = tail.previous;
			tail.next = null;
			--size;
			return tempNode.element;
		}
	}

	/**
	 * Clear the list
	 */
	public void clear() {
		size = 0;
		head = tail = null;
	}

	/**
	 * Return true if this list contains the element o
	 */
	public boolean contains(Object e) {
		if (size() == 0) return false;
		for (E element : this) {
			if (element.equals(e)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the element from this list at the specified index
	 */
	public E get(int index) {
		if (index < 0 || index >= size) throw new IndexOutOfBoundsException();

		Node<E> current;

		if (index > size() / 2) {
			current = tail;
			for (int i = size() - 1; i > index; i--) {
				current = current.previous;
			}
		} else {
			current = head;
			for (int i = 0; i < index; i++) {
				current = current.next;
			}
		}
		return current.element;
	}

	/**
	 * Return the index of the head matching element in this list. Return -1 if
	 * no match.
	 */
	public int indexOf(Object e) {
		int index = -1;
		Node<E> current = head;
		for (int i = 0; i < size(); i++) {
			if (current.element.equals(e)) {
				index = i;
				break;
			}
			current = current.next;
		}
		return index;
	}

	/**
	 * Return the index of the last matching element in this list Return -1 if
	 * no match.
	 */
	public int lastIndexOf(Object e) {
		int index = -1;
		Node<E> current = tail;
		for (int i = size() - 1; i >= 0; i--) {
			if (current.element.equals(e)) {
				index = i;
				break;
			}
			current = current.previous;
		}
		return index;
	}

	/**
	 * Replace the element at the specified position in this list with the
	 * specified element.
	 */
	public E set(int index, E e) {
		if (size == 0 || index >= size)
			throw new IndexOutOfBoundsException("List is empty or the index does not exist in this list.");
		Node<E> current;

		if (index > size / 2) {
			current = tail;
			for (int i = size - 1; i > index; i--)
				current = current.previous;
		} else {
			current = head;
			for (int i = 0; i < index; i++) {
				current = current.next;
			}
		}
		E replacedElement = current.element;
		current.element = e;

		return replacedElement;
	}

	public ListIterator<E> listIterator() {
		return new LinkedListIterator();
	}

	public ListIterator<E> listIterator(int index) {
		return new LinkedListIterator(index);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Iterator<E> iterator() {
		return listIterator();
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder("[");

		Node<E> current = head;
		for (int i = 0; i < size; i++) {
			result.append(current.element);
			current = current.next;
			if (current != null) {
				result.append(", "); // Separate two elements with a comma
			}

		}
		result.append("]"); // Insert the closing ] in the string

		return result.toString();
	}

	private class LinkedListIterator implements java.util.ListIterator<E> {
		private Node<E> current = head; // Current index
		private int cursor;
		private boolean addOrRemoveCalled, nextCalled, previousCalled;

		public LinkedListIterator() {
		}

		public LinkedListIterator(int index) {
			if (index == size()) {
				cursor = index;
				current = null;
			} else if (index > size() / 2) {
				cursor = size() - 1;
				current = tail;
				for (int i = size - 1; i > index; i--)
					this.previous();
			} else {
				cursor = 0;
				for (int i = 0; i < index; i++)
					this.next();
			}
		}

		// This method was defined in the assignment but is not reachable in tests or in use.
		// This is because the class was defined to be private and the ListIterator class doest'n contain this method.
		// The method work as it should if the class is public but we decided to keep it if needed.
		public void setLast() {
			cursor = size() - 1;
			current = null;
		}

		@Override
		public int nextIndex() {
			if (hasNext()) return cursor;
			return size;
		}

		@Override
		public int previousIndex() {
			if (hasPrevious()) return cursor - 1;
			return -1;
		}

		@Override
		public void remove() {
			if ((nextCalled || previousCalled) && !addOrRemoveCalled) {
				if (size <= 1) {
					head = tail = null;
				} else if (cursor == size) {
					tail = tail.previous;
					tail.next = null;
				} else if (current.previous.previous != null) {
					current.previous.previous.next = current;
					current.previous = current.previous.previous;
				} else {
					current.previous = null;
					head = current;
				}

				--size;
				--cursor;

				nextCalled = previousCalled = false;
				addOrRemoveCalled = true;
			} else {
				throw new IllegalStateException("Neither next or previous have been called, " +
						"or remove or add have been called after the last call to next or previous.");
			}
		}

		@Override
		public void add(E e) {
			Node<E> insertedNode = new Node<>(e);
			if (size() == 0) {
				tail = head = current = insertedNode;
			} else {
				if (current == null) {
					insertedNode.previous = tail;
					tail.next = insertedNode;
					tail = insertedNode;
				} else {
					if (current.previous == null) {
						head = insertedNode;
					} else {
						current.previous.next = insertedNode;
						insertedNode.previous = current.previous.previous;
					}

					insertedNode.next = current;
					current.previous = insertedNode;
				}
			}
			nextCalled = previousCalled = false;
			addOrRemoveCalled = true;
			size++;
			current = insertedNode;
		}

		@Override
		public boolean hasNext() {
			return cursor != size();
		}

		@Override
		public E next() {
			if (hasNext()) {
				++cursor;
				nextCalled = true;
				previousCalled = addOrRemoveCalled = false;


				Node<E> nextNode = current;
				current = nextNode.next;
				return nextNode.element;
			} else {
				throw new NoSuchElementException("The iteration has no next element");
			}
		}

		@Override
		public boolean hasPrevious() {
			return cursor != 0;
		}

		@Override
		public E previous() {
			if (hasPrevious()) {
				if (cursor == size()) {
					current = tail;
				} else {
					current = current.previous;
				}
				--cursor;
				previousCalled = true;
				nextCalled = addOrRemoveCalled = false;

				return current.element;
			} else {
				throw new NoSuchElementException("The iteration has no previous element");
			}
		}

		@Override
		public void set(E e) {
			if (nextCalled && !addOrRemoveCalled) {
				current.previous.element = e;
			} else if (previousCalled && !addOrRemoveCalled) {
				current.element = e;
			} else {
				throw new IllegalStateException("Neither next or previous have been called, " +
						"or remove or add have been called after the last call to next or previous.");
			}
		}
	}

	private static class Node<E> {
		E element;
		Node<E> next;
		Node<E> previous;

		public Node(E o) {
			element = o;
		}
	}
}