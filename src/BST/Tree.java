package BST;

import java.util.ArrayList;
import java.util.Collection;

public interface Tree<E> extends Collection<E> {

    /**
     * Return true if the element is in the tree
     */
    public boolean search(E e);

    /**
     * Insert element e into the binary tree
     * Return true if the element is inserted successfully
     */
    public boolean insert(E e);

    /**
     * Delete the specified element from the tree
     * Return true if the element is deleted successfully
     */
    public boolean delete(E e);

    /**
     * Get the number of elements in the tree
     */
    public int getSize();

    public ArrayList<E> inorderNoRecursion();

    /**
     * Inorder traversal from the root
     */
    public default void inorder() {
    }

    /**
     * Postorder traversal from the root
     */
    public default void postorder() {
    }

    /**
     * Preorder traversal from the root
     */
    public default void preorder() {
    }

    @Override
    /** Return true if the tree is empty */
    public default boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public default boolean contains(Object e) {
        return search((E) e);
    }

    @Override
    public default boolean add(E e) {
        return insert(e);
    }

    @Override
    public default boolean remove(Object e) {
        return delete((E) e);
    }

    @Override
    public default int size() {
        return getSize();
    }

    @Override
    public default boolean containsAll(Collection<?> c) {
        for (Object e : c) {
            if (!search((E) e)) return false;
        }
        return true;
    }

    @Override
    public default boolean addAll(Collection<? extends E> c) {
        boolean isModified = false;
        for (E e : c) {
            insert(e);
            isModified = true;
        }
        return isModified;
    }

    @Override
    public default boolean removeAll(Collection<?> c) {
        boolean isModified = false;
        for (Object e : c) {
            if (contains(e)) {
                remove(e);
                isModified = true;
            }
        }
        return isModified;
    }

    @Override
    public default boolean retainAll(Collection<?> c) {
        boolean isModified = false;
        ArrayList newlist = new ArrayList();
        for (Object e : c) {
            if (contains(e)) {
                newlist.add(e);
                isModified = true;
            }
        }

        clear();
        addAll(newlist);
        return isModified;
    }

    @Override
    public default Object[] toArray() {
        Object[] obj = inorderNoRecursion().toArray();
        return obj;
    }

    @Override
    public default <T> T[] toArray(T[] array) {

        Class type = array.getClass().getComponentType();
        Object[] treeArray = toArray();

        for (int i = 0; i < size(); i++) {
            if (!type.isInstance(treeArray[i])) {
                throw new ArrayStoreException("Runtime type of the specified array is not a supertype of " +
                        "the runtime type of every element in this collection");
            }
        }

        if (array.length < size()) {
            array = (T[]) java.util.Arrays.copyOf(treeArray, size(), array.getClass());
        }
        else {
            for (int i = 0; i < size(); i++) {
                array[i] = (T) treeArray[i];
            }
        }

        return array;
    }
}
