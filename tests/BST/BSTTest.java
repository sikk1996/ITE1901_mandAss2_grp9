package BST;

import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.*;

public class BSTTest {
	private BST<Integer> bst;

	@Before
	public void setUp() {
		bst = new BST<Integer>();
	}

	@Test
	public void search_ElementExist_ReturnsTrue() {
		bst.insert(4);
		bst.insert(2);
		bst.insert(10);
		bst.insert(8);

		assertTrue(bst.search(10));
	}

	@Test
	public void insert_DuplicateNode_returnsFalse(){
		assertTrue(bst.insert(1));
		assertFalse(bst.insert(1));
	}

	@Test
	public void isEmpty_EmptyTree_returnsTrue(){
		assertTrue(bst.isEmpty());
	}

	@Test
	public void isEmpty_NotEmptyTree_returnsFalse(){
		bst.insert(1);
		assertFalse(bst.isEmpty());
	}

	@Test
	public void containsAll_ElementsExist_returnsTrue(){
		Character[] treeChars = {'d', 'a', 'b', 'c', 'e'};
		BST<Character> charTree = new BST<>(treeChars);

		Character[] chars = {'d', 'a'};
		ArrayList<Character> list = new ArrayList<>(Arrays.asList(chars));

		assertTrue(charTree.containsAll(list));
	}

	@Test
	public void containsAll_ElementsDontExist_returnsFalse(){
		Character[] treeChars = {'d', 'a', 'b', 'c', 'e'};
		BST<Character> charTree = new BST<>(treeChars);

		Character[] chars = {'g', 'm'};
		ArrayList<Character> existList = new ArrayList<>(Arrays.asList(chars));

		assertFalse(charTree.containsAll(existList));
	}

	@Test
	public void removeAll_ElementsExistInTree_returnsTrue(){
		Character[] treeChars = {'d', 'a', 'b', 'c', 'e'};
		BST<Character> charTree = new BST<>(treeChars);
		Character[] removeChars = {'d', 'a'};
		ArrayList<Character> removeList = new ArrayList<>(Arrays.asList(removeChars));

		assertTrue(charTree.removeAll(removeList));
		assertFalse(charTree.search('d'));
		assertFalse(charTree.search('a'));
	}

	@Test
	public void removeAll_ElementsDontExistInTree_returnsFalse(){
		Character[] treeChars = {'d', 'a', 'b', 'c', 'e'};
		BST<Character> charTree = new BST<>(treeChars);
		Character[] removeChars = {'h', 'l'};
		ArrayList<Character> removeList = new ArrayList<>(Arrays.asList(removeChars));

		assertFalse(charTree.removeAll(removeList));
	}


	@Test
	public void toArray_returnsArrayOfObjectInorder(){
		Integer[] numbers = {10, 1, 5, 3, 11, 15, 28};
		BST<Integer> intTree = new BST<>(numbers);

		Object[] array = intTree.toArray();

		Integer[] expected = {1, 3, 5, 10, 11, 15, 28};
		for(int i = 0; i < array.length; i++) {
			assertSame(array[i], expected[i]);
		}
	}

	@Test
	public void toArray_withArrayAsParamter_returnsArrayContainingTreeInorder(){
		Integer[] numbers = {10, 1, 5, 3, 11, 15, 28};
		BST<Integer> intTree = new BST<>(numbers);
		Integer[] array = new Integer[numbers.length];

	    intTree.toArray(array);

		Integer[] expected = {1, 3, 5, 10, 11, 15, 28};
		assertArrayEquals(expected, array);
	}

	@Test(expected = ArrayStoreException.class)
	public void toArray_IntTree_ParamAsArrayOfStrings_returnsException_(){
		Integer[] numbers = {10, 1, 5, 3, 11, 15, 28};
		BST<Integer> intTree = new BST<>(numbers);

		intTree.toArray(new String[]{"test", "test"});
	}

	@Test
	public void toArray_sizezoor_(){
		Integer[] numbers = {10, 1, 5, 3, 11, 15, 28, 9, 2, 22};
		BST<Integer> intTree = new BST<>(numbers);
		Integer[] array = new Integer[5];

		assertEquals(10, intTree.toArray(array).length);
	}


	@Test
	public void inorderNoRecursion_returnsTreeInOrder() {
		Integer[] numbers = {60, 45, 10, 1, 5, 3, 11, 15, 28};
		BST<Integer> intTree = new BST<>(numbers);

		Integer[] expectedNumbers = {1, 3, 5, 10, 11, 15, 28, 45, 60};

		assertEquals(Arrays.asList(expectedNumbers), intTree.inorderNoRecursion());
	}

	@Test
	public void getNoOfLeaves_add4Leaves_returnsFireLeaves() {
		Integer[] numbers = {60, 55, 45, 47, 59, 100, 76, 107, 101};
		BST<Integer> intTree = new BST<>(numbers);
		assertEquals(4, intTree.getNoOfLeaves());
	}

	@Test
	public void getNoOfNonLeaves() {
		Integer[] numbers = {60, 55, 45, 47, 59, 100, 76, 107, 101};
		BST<Integer> intTree = new BST<>(numbers);
		assertEquals(5, intTree.getNoOfNonLeaves());
	}

	@Test
	public void isPerfect_returnsTrue() {
		Integer[] numbers = {60, 55, 45, 56, 100, 76, 107};
		BST<Integer> intTree = new BST<>(numbers);
		assertTrue(intTree.isPerfect());
	}
	@Test
	public void isPerfect_returnsFalse() {
		Integer[] numbers2 = {60, 55, 45, 56, 100, 76, 107, 103};
		BST<Integer> intTree2 = new BST<>(numbers2);
		assertFalse(intTree2.isPerfect());
	}

	@Test
	public void retainAll_returnsTreeWithCommonElements(){
		Integer[] numbers = {1, 5, 3, 11, 15, 28};
		BST<Integer> intTree = new BST<>(numbers);

		Integer[] retainList = {1, 5, 100, 15};
		ArrayList<Integer> expected = new ArrayList<>();
		expected.add(1);
		expected.add(5);
		expected.add(15);

		assertTrue(intTree.retainAll(Arrays.asList(retainList)));
		assertEquals(expected, intTree.inorderNoRecursion());
	}

	@Test
	public void inorder_ReturnsTreeElementsInorder(){
		OutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		System.setOut(ps);
		Integer[] numbers = {3, 6, 1, 5, 4, 8, 2};
		BST<Integer> intTree = new BST<>(numbers);

		intTree.inorder();

		assertEquals("1 2 3 4 5 6 8 ", os.toString());
	}

	@Test
	public void postorder_ReturnsTreeElementsPostorder(){
		OutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		System.setOut(ps);
		Integer[] numbers = {3, 6, 1, 5, 4, 8, 2};
		BST<Integer> intTree = new BST<>(numbers);

		intTree.postorder();

		assertEquals("2 1 4 5 8 6 3 ", os.toString());
	}

	@Test
	public void preorder_ReturnsTreeElementsPreorder(){
		OutputStream os = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(os);
		System.setOut(ps);
		Integer[] numbers = {3, 6, 1, 5, 4, 8, 2};
		BST<Integer> intTree = new BST<>(numbers);

		intTree.preorder();

		assertEquals("3 1 2 6 5 4 8 ", os.toString());
	}


	/*	Iterator */


	@Test
	public void iterator_returnsInorderIterator(){
		Iterator iterator = bst.iterator();

		assertEquals("class BST.BST$InorderIterator", iterator.getClass().toString());
	}

	@Test
	public void hasNext_FromInitializeState_returnsTrue(){
		Integer[] numbers = {6, 4, 3, 11, 2, 12, 1};
		bst = new BST<>(numbers);

		Iterator iterator = bst.iterator();

		assertEquals(true, iterator.hasNext());
	}

	@Test
	public void hasNext_EndOfTree_returnsFalse(){
		Integer[] numbers = {6,1};
		bst = new BST<>(numbers);

		Iterator iterator = bst.iterator();
		iterator.next();
		iterator.next();
		assertEquals(false, iterator.hasNext());
	}

	@Test
	public void next_FromInitializeState_returnsLowestNumber(){
		Integer[] numbers = {6, 4, 3, 11, 5, 12, 1};
		bst = new BST<>(numbers);

		Iterator iterator = bst.iterator();

		assertEquals(1, iterator.next());
		assertEquals(3, iterator.next());
	}


	@Test(expected = IllegalStateException.class)
	public void remove_nextHasNotBeenCalled_returnsIllegalStateException(){
		Iterator iterator = bst.iterator();
		iterator.remove();
	}

	@Test
	public void remove_elementShouldNotExists(){
		Integer[] numbers = {6, 4, 3, 11, 5, 12, 1};
		bst = new BST<>(numbers);

		Iterator iterator = bst.iterator();

		assertTrue(bst.search(1));
		iterator.next();
		iterator.remove();
		assertFalse(bst.search(1));
	}

}