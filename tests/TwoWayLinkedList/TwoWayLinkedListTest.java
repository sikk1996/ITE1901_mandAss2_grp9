package TwoWayLinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import java.util.*;
import static org.junit.Assert.*;

public class TwoWayLinkedListTest {
	private TwoWayLinkedList<? super Object> linkedList;

	@Before
	public void setUp() {
		linkedList = new TwoWayLinkedList<>();
	}

	@Test
	public void initializeListWithArrayOfObjects() {
		String[] arrayList = {"test", "test2", "test3", "test4"};
		linkedList = new TwoWayLinkedList<Object>(arrayList);
		assertEquals("[test, test2, test3, test4]", linkedList.toString());
	}

	@Test
	public void isNotEmpty() {
		Object o = new Object();
		linkedList.add(o);
		assertFalse(linkedList.isEmpty());
	}

	@Test(expected = NoSuchElementException.class)
	public void removeFirst() {
		assertNull(linkedList.removeFirst());
		linkedList.removeFirst(); // Throws NoSuchElementException

		linkedList.add("test");
		assertEquals("test", linkedList.removeFirst());

		linkedList.add("tester");
		linkedList.add("teste2");
		assertEquals("tester", linkedList.removeFirst());
	}

	@Test(expected = NoSuchElementException.class)
	public void removeLast() {
		linkedList.add("test1");
		linkedList.add("test2");

		assertEquals("test2",linkedList.removeLast());
		assertEquals("test1", linkedList.removeLast());
		linkedList.removeLast(); // Throws NoSuchElementException
	}

	@Test
	public void removeIndex() {
		linkedList.add("test");
		linkedList.add("test1");
		linkedList.add("test2");
		linkedList.add("test4");
		linkedList.add("test5");

		linkedList.remove(0);
		assertEquals(4, linkedList.size());
		linkedList.remove(3);
		assertEquals(3, linkedList.size());
		assertEquals("[test1, test2, test4]", linkedList.toString());
	}

	@Test
	public void addFirstgetFirst() {
		String[] arrayList = {"test", "test2", "test3", "test4"};
		linkedList = new TwoWayLinkedList<Object>(arrayList);
		linkedList.addFirst("testAddFirst");
		assertEquals("testAddFirst", linkedList.getFirst());
	}

	@Test
	public void addLastGetLast() {
		String[] arrayList = {"test", "test2", "test3", "test4"};
		linkedList = new TwoWayLinkedList<Object>(arrayList);
		linkedList.addLast("testAddLast");
		assertEquals("testAddLast", linkedList.getLast());
	}

	@Test
	public void firstIndexOflastIndexOf() {
		assertEquals(-1,linkedList.lastIndexOf(5));

		linkedList.add(1);
		linkedList.add(2);
		linkedList.add(1);
		linkedList.add(4);
		linkedList.add(5);

		assertEquals(0, linkedList.indexOf(1));
		assertEquals(3, linkedList.indexOf(4));
		assertEquals(4, linkedList.indexOf(5));
		assertEquals(4, linkedList.lastIndexOf(5));
		assertEquals(2, linkedList.lastIndexOf(1));
		assertEquals(-1, linkedList.lastIndexOf(10));
	}

	@Test
	public void clearList_getSizeShouldReturnZero() {
		linkedList.add("test");
		linkedList.add("test2");
		linkedList.add("test3");
		assertEquals(3, linkedList.size());
		linkedList.clear();
		assertEquals(0, linkedList.size());
	}

	@Test
	public void contains() {
		String object = "test";
		linkedList.add(object);
		linkedList.add("Testest");
		assertTrue(linkedList.contains(object));
		assertFalse(linkedList.contains("nothing"));
	}

	@Test
	public void getIndex() {
		linkedList.add(9);
		linkedList.add(8);
		linkedList.add(7);
		assertEquals(9, linkedList.get(0));
		assertEquals(8, linkedList.get(1));
		assertEquals(7, linkedList.get(2));
	}

	@Test
	public void set_ReplaceElementAtSpecifiedPosition() {
		linkedList.add("abc");
		linkedList.add("def");
		linkedList.add("sdf");
		linkedList.add("fsd");
		assertEquals("def", linkedList.set(1, "123")); // Works
		assertEquals("abc", linkedList.set(0, "heihei")); //TODO fails here
		assertEquals("sdf", linkedList.set(2, "123")); // Works
	}

	@Test
	public void toString_addThreeObjectsToList() {
		addArrayElements(new Object[]{1, 2, 3});
		assertEquals("[1, 2, 3]", linkedList.toString());
		assertEquals(3, linkedList.set(2, "123"));
		assertEquals(1, linkedList.set(0, "123"));
		assertEquals(2, linkedList.set(1, "123"));
	}

	@Test
	public void containsAllStrings_returnsTrue() {
		addArrayElements(new Object[]{1, 2, 3, 4});
		Integer[] integers = {1, 2, 3};
		List<Integer> list = Arrays.asList(integers);
		assertTrue(linkedList.containsAll(list));
	}

	@Test
	public void AddNewElementToSpecifiedIndex() {
		addArrayElements(new String[]{"test", "test2", "test3", "test4"});
		linkedList.add(0,"AddIndex0");
		linkedList.add(1, "AddIndex1");
		linkedList.add(4, "AddIndex5");
		assertEquals("[AddIndex0, AddIndex1, test, test2, AddIndex5, test3, test4]", linkedList.toString());
	}

	@Test
	public void forEach_toArrayList() {
		addArrayElements(new Object[]{1,2,3,4,5});
		ArrayList<Object> arrayList = new ArrayList<>();
		for (Object e : linkedList) {
			arrayList.add(e);
		}
		assertEquals("[1, 2, 3, 4, 5]", arrayList.toString());
	}

	@Test
	public void getIntegerElements() {
		addArrayElements(new Integer[]{1, 2, 3, 4, 5});
		assertEquals(1, linkedList.get(0));
		assertEquals(2, linkedList.get(1));
		assertEquals(3, linkedList.get(2));
		assertEquals(4, linkedList.get(3));
		assertEquals(5, linkedList.get(4));
	}

	@Test
	public void containElement() {
		addArrayElements(new Integer[]{1, 2, 3, 4, 5});
		assertTrue(linkedList.contains(1));
		assertTrue(linkedList.contains(2));
		assertTrue(linkedList.contains(5));
		assertFalse(linkedList.contains(0));
		assertFalse(linkedList.contains(6));
	}

	@Test
	public void addAll() {
		ArrayList<String> arrayList = new ArrayList<>();
		arrayList.add("Test1");
		arrayList.add("Test2");
		arrayList.add("Test3");
		linkedList.addAll(arrayList);
		assertEquals("[Test1, Test2, Test3]", arrayList.toString());
	}

	@Test
	public void ifOneElementFirstIsLast() {
		linkedList.add("test");
		assertEquals(linkedList.getFirst(), linkedList.getLast());
	}

	@Test (expected = NoSuchElementException.class)
	public void ifsizeIsNull_getFirstAndGetLastReturnsNull() {
		assertNull(linkedList.getFirst());
		assertNull(linkedList.getLast());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void indexOutOfBoundsWhenSize0() {
		ExpectedException.none().expect(IndexOutOfBoundsException.class);
		linkedList.get(0);
	}

	@Test
	public void iterator_hasNotNext() {
		Iterator iterator = linkedList.listIterator();
		assertFalse(iterator.hasNext());
	}

	@Test
	public void iterator_hasNext() {
		linkedList.add(new Object());
		Iterator iterator = linkedList.listIterator();
		assertTrue(iterator.hasNext());
	}

	@Test
	public void iterator_hasNotPrevious() {
		Iterator iterator = linkedList.listIterator();
		assertFalse(((ListIterator) iterator).hasPrevious());
	}

	@Test
	public void removeObjectThatExistInList_shouldReturnTrue() {
		addArrayElements(new Integer[]{1, 2, 2, 3, 4});
		assertTrue(linkedList.remove(new Integer(2)));
	}

	@Test
	public void removeObjectThatDoesNotExistInList_shouldReturnFalse() {
		assertFalse(linkedList.remove(new Integer(10)));
	}

	@Test
	public void removeALL() {
		addArrayElements(new String[]{"Hello", "there", "how", "is", "life"});
		String[] removeList = {"there", "how", "is", "life"};
		List<String> collection = Arrays.asList(removeList);

		assertEquals(5, linkedList.size());
		assertTrue(linkedList.removeAll(collection));
		assertEquals(1, linkedList.size());
	}

	@Test
	public void retainAll() {
		addArrayElements(new String[]{"hello", "there", "boij", "how", "is", "life"});
		String[] retainList = {"hello", "life", "boij"};
		List<String> collection = Arrays.asList(retainList);

		assertTrue(linkedList.retainAll(collection));
		assertEquals("[hello, boij, life]", linkedList.toString());
		assertEquals(3, linkedList.size());
	}

	@Test
	public void nextIndex_whenCursorIs2_returns2() {
		addArrayElements(new Object[]{"String1", "String2", "String3", "String4", "String5"});
		ListIterator listIterator = linkedList.listIterator(2);
		assertEquals(2, listIterator.nextIndex());
	}

	@Test
	public void nextIndex_whenCursorIs3_returns3() {
		addArrayElements(new Object[]{"String1", "String2", "String3", "String4", "String5"});
		ListIterator listIterator = linkedList.listIterator(3);
		assertEquals(3, listIterator.nextIndex());
	}

	@Test
	public void nextIndex_whenCursorIs4_returns4() {
		addArrayElements(new Object[]{"String1", "String2", "String3", "String4", "String5"});
		ListIterator listIterator = linkedList.listIterator(5);
		assertEquals(5, listIterator.nextIndex()); // Returns list size when at the end of the list
	}

	@Test
	public void previousIndex_whenCursorIs2_returns1() {
		addArrayElements(new Object[]{"String1", "String2", "String3", "String4", "String5"});
		ListIterator listIterator = linkedList.listIterator(2);
		assertEquals(1, listIterator.previousIndex());
	}

	@Test
	public void previousIndex_whenCursorIs4_returns3() {
		addArrayElements(new Object[]{"String1", "String2", "String3", "String4", "String5"});
		ListIterator listIterator = linkedList.listIterator(4);
		assertEquals(3, listIterator.previousIndex());
	}

	@Test
	public void previousIndex_whenCursorIs0_returnsMinus1() {
		addArrayElements(new Object[]{"String1", "String2", "String3", "String4", "String5"});
		ListIterator listIterator = linkedList.listIterator(0);
		assertEquals(-1, listIterator.previousIndex()); // Returns list size when at the beginning of the list
	}

	@Test(expected = IllegalStateException.class)
	public void remove_noNextOrPreviousCalled_throwsIllegalStateException() {
		ListIterator listIterator = linkedList.listIterator();
		listIterator.remove();
	}

	@Test
	public void remove_firstElementWithNext_isRemoved() {
		addArrayElements(new Object[]{"String1", "String2", "String3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.remove();
		assertEquals("[String2, String3]", linkedList.toString());
	}

	@Test
	public void remove_middleElementWithNext_isRemoved() {
		addArrayElements(new Object[]{"String1", "String2", "String3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.next();
		listIterator.remove();
		assertEquals("[String1, String3]", linkedList.toString());
	}

	@Test
	public void remove_lastElementWithNext_isRemoved() {
		addArrayElements(new Object[]{"String1", "String2", "String3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.next();
		listIterator.next();
		listIterator.remove();
		assertEquals("[String1, String2]", linkedList.toString());
	}

	@Test
	public void remove_sizeIs1_isRemoved() {
		linkedList.add("String1");
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.remove();
		assertEquals("[]", linkedList.toString());
	}

	@Test
	public void add_currentIsNull_tailIsTheInsertedNode() {
		addArrayElements(new Object[]{"String1", "String2", "String3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.next();
		listIterator.next();
		listIterator.add("NewString");
		assertEquals("[String1, String2, String3, NewString]", linkedList.toString());
	}

	@Test
	public void add_currentIsNotNull_insertedNodeShouldAppearInBetween() {
		addArrayElements(new Object[]{"String1", "String2", "String3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.next();
		listIterator.add("NewString");
		assertEquals("[String1, String2, NewString, String3]", linkedList.toString());
	}

	@Test
	public void add_listIsEmpty_insertedNodesShouldBeInsertedInFrontOfEachOther() {
		ListIterator listIterator = linkedList.listIterator();
		listIterator.add("last");
		listIterator.add("middle");
		listIterator.add("first");
		assertEquals("[first, middle, last]", linkedList.toString());
	}

	@Test
	public void hasNext_listIsEmpty_returnsFalse() {
		ListIterator listIterator = linkedList.listIterator();
		assertFalse(listIterator.hasNext());
	}

	@Test
	public void hasNext_sizeIs1WhenCursorIsZero_returnsTrue() {
		linkedList.add("first element");
		ListIterator listIterator = linkedList.listIterator();
		assertTrue(listIterator.hasNext());
	}

	@Test
	public void next_sizeIs1_returnsFirstElement() {
		linkedList.add("first element");
		ListIterator listIterator = linkedList.listIterator();
		assertEquals("first element", listIterator.next());
	}

	@Test(expected = NoSuchElementException.class)
	public void next_sizeIs0_throwsNoSuchElementException() {
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
	}

	@Test
	public void hasPrevious_listIsEmpty_returnsFalse() {
		ListIterator listIterator = linkedList.listIterator();
		assertFalse(listIterator.hasPrevious());
	}

	@Test
	public void hasNext_sizeIs1WhenCursorIsOne_returnsTrue() {
		linkedList.add("first element");
		ListIterator listIterator = linkedList.listIterator(1);
		assertTrue(listIterator.hasPrevious());
	}

	@Test
	public void previous_cursorIsSize_returnsFirstElement() {
		linkedList.add("first element");
		ListIterator listIterator = linkedList.listIterator(1);
		assertEquals("first element", listIterator.previous());
	}

	@Test(expected = NoSuchElementException.class)
	public void previous_sizeIs0_throwsNoSuchElementException() {
		ListIterator listIterator = linkedList.listIterator();
		listIterator.previous();
	}

	@Test
	public void set_nextIsCalled_firstElementShouldBeNewElement() {
		addArrayElements(new Object[]{"Element1", "Element2", "Element3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.next();
		listIterator.set("New Element");
		assertEquals("[New Element, Element2, Element3]", linkedList.toString());
	}

	@Test
	public void set_previousIsCalled_firstElementShouldBeNewElement() {
		addArrayElements(new Object[]{"Element1", "Element2", "Element3"});
		ListIterator listIterator = linkedList.listIterator(3);
		listIterator.previous();
		listIterator.set("New Element");
		assertEquals("[Element1, Element2, New Element]", linkedList.toString());
	}

	@Test(expected = IllegalStateException.class)
	public void set_nextIsNotCalled_throwsIllegalStateException() {
		addArrayElements(new Object[]{"Element1", "Element2", "Element3"});
		ListIterator listIterator = linkedList.listIterator();
		listIterator.set("New Element");
	}

	@Test
	public void toArrayNonGeneric_ShouldReturnArrayOfObject() {
		addArrayElements(new Integer[]{1, 2, 2, 3, 4});
		assertEquals("java.lang.Object[]", linkedList.toArray().getClass().getCanonicalName());
	}

	@Test
	public void toArray_stringArrayLength1_shouldBeAdjusted() {
		addArrayElements(new String[]{"Element1", "Element2", "Element3"});
		String[] stringArray = new String[1];
		stringArray = linkedList.toArray(stringArray);
		assertArrayEquals(new String[]{"Element1", "Element2", "Element3"}, stringArray);
	}

	@Test(expected = ArrayStoreException.class)
	public void toArray_differentTypes_throwsArrayStoreException() {
		linkedList.add("String1");
		linkedList.add("String2");
		linkedList.add(1);
		linkedList.add(2);
		String[] listArray = new String[2];
		linkedList.toArray(listArray); // Throws ArrayStoreException
	}

	private void addArrayElements(Object[] array) {
		for (Object e : array) {
			linkedList.add(e);
		}
	}

	//TODO: Make help functions to shorten code!
}

















